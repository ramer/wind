package io.github.ramerf.wind.core.handler.typehandler;

import io.github.ramerf.wind.core.executor.DataAccessException;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

/**
 * {@literal java:Collection<String> <=> jdbc:逗号分割的字符串}.
 *
 * @since 2024.04.20
 * @author ramer
 */
public class StringArrayToCommaTypeHandler implements ITypeHandler<Collection<String>, String> {
  private static final Pattern COMMA = Pattern.compile(",");

  @Override
  public Object convertToJdbc(
      Collection<String> javaVal, final Field field, @Nonnull final PreparedStatement ps) {
    if (javaVal == null) {
      return null;
    }
    try {
      final Connection connection = ps.getConnection();
      return javaVal.isEmpty()
          ? ""
          : javaVal.stream().map(String::valueOf).collect(Collectors.joining(",", "", ","));
    } catch (SQLException e) {
      throw new DataAccessException(e);
    }
  }

  @Override
  public Collection<String> convertFromJdbc(
      final String jdbcVal, final Object defaultValue, final Field field) {
    if (defaultValue == null) {
      return List.class.isAssignableFrom(field.getType())
          ? (jdbcVal == null
              ? new ArrayList<>()
              : COMMA.splitAsStream(jdbcVal).collect(Collectors.toList()))
          : (jdbcVal == null
              ? new HashSet<>()
              : COMMA.splitAsStream(jdbcVal).collect(Collectors.toSet()));
    }
    @SuppressWarnings("unchecked")
    final Collection<String> initial = (Collection<String>) defaultValue;
    initial.clear();
    initial.addAll(COMMA.splitAsStream(jdbcVal).collect(Collectors.toList()));
    return initial;
  }
}
