package io.github.ramerf.wind.core.plugin;

import io.github.ramerf.wind.core.reflect.ExceptionUtil;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class JdkProxyDaoInterceptor implements InvocationHandler {

  private final Object target;
  private final DaoInterceptorChain interceptorChain;

  public JdkProxyDaoInterceptor(Object target, DaoInterceptorChain interceptorChain) {
    this.target = target;
    this.interceptorChain = interceptorChain;
  }

  @Override
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    try {
      final String name = method.getName();
      if (Plugins.QUERY_METHODS_DAO.contains(name) || Plugins.UPDATE_METHODS_DAO.contains(name)) {
        return interceptorChain.proceed(new Invocation(target, method, args, interceptorChain));
      }
      return method.invoke(target, method, args);
    } catch (Exception e) {
      throw ExceptionUtil.unwrapThrowable(e);
    }
  }
}
