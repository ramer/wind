package io.github.ramerf.wind.core.handler.typehandler;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import javax.annotation.Nonnull;

/**
 * {@literal java:Enum <=> jdbc:varchar}.
 *
 * @since 2024.08.29
 * @author ramer
 */
@SuppressWarnings("rawtypes")
public class EnumTypeHandler implements ITypeHandler<Enum, Object> {
  @Override
  public Object convertToJdbc(
      Enum enumuration, final Field field, @Nonnull final PreparedStatement ps) {
    return enumuration != null ? enumuration.name() : null;
  }

  @Override
  @SuppressWarnings("unchecked")
  public Enum convertFromJdbc(final Object value, final Object defaultValue, final Field field) {
    return value != null ? Enum.valueOf((Class<Enum>) field.getType(), value.toString()) : null;
  }
}
