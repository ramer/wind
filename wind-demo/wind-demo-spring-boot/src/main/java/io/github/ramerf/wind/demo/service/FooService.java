package io.github.ramerf.wind.demo.service;

import io.github.ramerf.wind.core.service.BaseService;
import io.github.ramerf.wind.demo.entity.pojo.Foo;

/**
 * @author ramer
 * @since 2022.06.05
 */
public interface FooService extends BaseService<Foo, Long> {}
