package io.github.ramerf.wind.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The type Demo spring boot application.
 *
 * @since 2022.06.18
 * @author ramer
 */
@SpringBootApplication
public class DemoSpringBootApplication {
  public static void main(String[] args) {
    SpringApplication.run(DemoSpringBootApplication.class, args);
  }
}
