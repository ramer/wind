package io.github.ramerf.wind.demo.service;

import io.github.ramerf.wind.core.service.BaseService;
import io.github.ramerf.wind.demo.entity.Foo;

/**
 * The interface Foo service.
 *
 * @since 2022.06.19
 * @author ramer
 */
public interface FooService extends BaseService<Foo, Long> {
  void foo(final String string);
}
